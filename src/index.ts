import express, { Application } from "express";

const app: Application = express();

app.get("/", (req, res) => {
  res.send("Hello, world!");
});

const PORT = 3000;
app.listen(PORT, () =>
  console.log(`🚀 Server ready on http://localhost:${PORT}`)
);
